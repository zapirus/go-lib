package main

import (
	"flag"
	"log"

	"github.com/BurntSushi/toml"

	"go-library/internal/handlers"
)

var (
	configPath string
)

func init() {
	flag.StringVar(&configPath, "config-path", "config/config.toml", "")
}

func main() {
	flag.Parse()
	config := handlers.NewConfig()
	_, err := toml.DecodeFile(configPath, config)
	if err != nil {
		log.Fatalln(err)
	}
	s := handlers.New(config)
	s.Run()

}
