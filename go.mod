module go-library

go 1.20

require (
	github.com/BurntSushi/toml v1.3.2 // indirect
	github.com/brianvoe/gofakeit v3.18.0+incompatible // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/jmoiron/sqlx v1.3.5 // indirect
	github.com/karlseguin/gofake v0.0.0-20200216034827-35022ec4d9bf // indirect
	github.com/sirupsen/logrus v1.9.3 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)
