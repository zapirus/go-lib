package main

import "go-library/internal/models"

//go:generate swagger generate spec -o /home/huawei/Документы/golang/go-library/public/swagger.json --scan-models

// swagger:route POST /adduser users usersPostRequest
// Добавление пользователя в библиотеку.
// responses:
//	200: usersPostResponse

type addUser struct {
	Name string `json:"name"`
}

// swagger:parameters  usersPostRequest
type usersPostRequest struct {
	// Имя пользователя.
	// Required: true
	// In: body
	Query addUser
}

// swagger:response usersPostResponse
type usersPostResponse struct {
	// in: body
	Body models.User
}

// swagger:route GET /getusers users usersGetRequest
// Получение всех пользователей.
// responses:
//	200: usersGetResponse

// swagger:parameters  usersGetRequest

// swagger:response usersGetResponse
type usersGetResponse struct {
	// in: body
	Body []models.User
}

// swagger:route POST /addauthor author authorPostRequest
// Добавление автора в библиотеку.
// responses:
//	200: authorPostResponse

type addAuthor struct {
	Name string `json:"name"`
}

// swagger:parameters  authorPostRequest
type authorPostRequest struct {
	// Имя автора.
	// Required: true
	// In: body
	Query addAuthor
}

// swagger:response authorPostResponse
type authorPostResponse struct {
	// in: body
	Body models.Author
}

// swagger:route GET /getauthor author authorGetRequest
// Получение всех авторов.
// responses:
//	200: authorGetResponse

// swagger:parameters  authorGetRequest

// swagger:response authorGetResponse
type authorGetResponse struct {
	// in: body
	Body []models.Author
}

// swagger:route POST /addbook books bookPostRequest
// Добавление книги в библиотеку.
// responses:
//	200: bookPostResponse

type addBook struct {
	AuthorId int    `json:"author_id"`
	Title    string `json:"title"`
}

// swagger:parameters  bookPostRequest
type bookPostRequest struct {
	// ID автора.
	// Required: true
	// In: body
	Query addBook
}

// swagger:response bookPostResponse
type bookPostResponse struct {
	// in: body
	Body models.Book
}

// swagger:route GET /getbooks books bookGetRequest
// Получение всех книг.
// responses:
//	200: bookGetResponse

// swagger:parameters  bookGetRequest

// swagger:response bookGetResponse
type bookGetResponse struct {
	// in: body
	Body []models.Book
}

// swagger:route POST /topauthors author authorTopGetRequest
// Наиболее читаемые авторы.
// responses:
//	200: authorTopGetResponse

type topAuthor struct {
	Count int `json:"count"`
}

// swagger:parameters  authorTopGetRequest
type authorTopGetRequest struct {
	// Количество авторов (по дефолту - 10).
	// In: body
	Query topAuthor
}

// swagger:response authorTopGetResponse
type authorTopGetResponse struct {
	// in: body
	Body []models.Author
}

// swagger:route POST /rented library bookRentPostRequest
// Взятие книги в аренду.
// responses:
//	200: bookRentPostResponse

type rentBook struct {
	UserId int    `json:"user_id"`
	Title  string `json:"title"`
}

// swagger:parameters  bookRentPostRequest
type bookRentPostRequest struct {
	// ID автора.
	// Required: true
	// In: body
	Query rentBook
}

// swagger:response bookRentPostResponse
type bookRentPostResponse struct {
	// in: body
}

// swagger:route POST /return library bookReturnPostRequest
// Возврат книги.
// responses:
//	200: bookRentPostResponse

// swagger:parameters  bookReturnPostRequest
type bookReturnPostRequest struct {
	// ID автора.
	// Required: true
	// In: body
	Query rentBook
}

// swagger:response bookReturnPostResponse
type bookReturnPostResponse struct {
	// in: body
}
