package service

import (
	"database/sql"
	"errors"
	"fmt"

	"go-library/internal/models"
	"go-library/internal/repository"
)

type Service struct {
	r *repository.Repository
}

func NewService() *Service {
	return &Service{}
}

// user

func (s *Service) GetUser(title string) (models.User, error) {
	user, err := s.r.GetUser(title)
	if err != nil {
		return models.User{}, err
	}
	return user, nil
}

func (s *Service) AddUser(user models.User) (models.User, error) {
	err := s.r.AddUser(user.Name)
	if err != nil {
		return models.User{}, err
	}

	return user, nil
}

func (s *Service) GetUsers() ([]models.User, error) {
	users, err := s.r.GetUsers()
	if err != nil {
		return []models.User{}, err
	}

	return users, nil
}

//author

func (s *Service) GetAuthor(title string) (models.Author, error) {
	author, err := s.r.GetAuthor(title)
	if err != nil {
		return models.Author{}, err
	}
	return author, nil
}

func (s *Service) GetAuthors() ([]models.Author, error) {
	author, err := s.r.GetAuthors()
	if err != nil {
		return []models.Author{}, err
	}

	return author, nil
}

func (s *Service) AddAuthor(author models.Author) error {
	err := s.r.AddAuthor(author.Name)
	if err != nil {
		return err
	}

	return nil
}

// book

func (s *Service) GetBook(title string) (models.Book, error) {
	book, err := s.r.GetBook(title)
	if err != nil {
		return models.Book{}, err
	}
	return book, nil
}

func (s *Service) GetBooks() ([]models.Book, error) {
	books, err := s.r.GetBooks()
	if err != nil {
		return []models.Book{}, err
	}

	return books, nil
}

func (s *Service) AddBook(authorID int, title string) error {
	err := s.r.AddBook(authorID, title)
	if err != nil {
		return err
	}

	return nil
}

func (s *Service) GetUserByID(userID int) (models.User, error) {
	user, err := s.r.GetUserById(userID)
	if err != nil {
		return models.User{}, err
	}
	return user, nil
}

func (s *Service) GetAuthorByID(authorID int) (models.Author, error) {
	author, err := s.r.GetAuthorById(authorID)
	if err != nil {
		return models.Author{}, err
	}
	return author, nil
}

func (s *Service) RentBook(userId int, title string) error {
	book, err := s.r.GetBook(title)
	if err != nil {
		return fmt.Errorf("book %s not found", title)
	}
	if book.RentUser != 0 {
		return fmt.Errorf("book %s is rent another user", title)
	}
	_, err = s.r.GetUserById(userId)
	if err != nil {
		return fmt.Errorf("user %d not found", userId)
	}
	err = s.r.RentBook(userId, title)
	if err != nil {
		return fmt.Errorf("error book rent")
	}
	return nil
}

func (s *Service) ReturnBook(userID int, title string) error {
	book, err := s.r.GetBook(title)
	if err != nil {
		return fmt.Errorf("book %s not found", title)
	}

	if book.RentUser != userID {
		return fmt.Errorf("user %d dont have %s book", userID, title)
	}

	err = s.r.ReturnBook(title)
	if err != nil {
		return fmt.Errorf("error book return")
	}
	return nil
}

func (s *Service) GetAuthorsTop(count int) ([]models.Author, error) {
	if count < 10 {
		count = 10
	}
	res, err := s.r.GetTopAuthors(count)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return nil, err
	}
	return res, nil
}
