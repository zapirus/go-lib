package models

type Author struct {
	Id        int    `json:"id,omitempty" db:"author_id"`
	Name      string `json:"name" db:"author_name"`
	ReadCount int    `json:"read_count,omitempty" db:"read_count"`
	Books     []Book `json:"book,omitempty" db:"books"`
}

type Book struct {
	Id       int    `json:"id,omitempty" db:"book_id"`
	Author   int    `json:"author_id,omitempty" db:"author_id"`
	Title    string `json:"title" db:"title"`
	RentUser int    `json:"user_id,omitempty" db:"user_id"`
}

type User struct {
	Id          int    `json:"id,omitempty" db:"user_id"`
	Name        string `json:"name" db:"user_name"`
	RentedBooks []Book ` json:"rented_books,omitempty" db:"rented_books"`
}

type Rent struct {
	UserId int    `json:"user_id"`
	Title  string `json:"title"`
}

type ValueCount struct {
	Count int `json:"count"`
}
