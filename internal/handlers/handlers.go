package handlers

import (
	"context"
	"encoding/json"
	"errors"
	"html/template"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/brianvoe/gofakeit"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"

	"go-library/internal/models"
	"go-library/internal/repository"
	"go-library/internal/service"
)

type APIServer struct {
	config  *Config
	router  *mux.Router
	logger  *logrus.Logger
	service service.Service
}

func New(config *Config) *APIServer {
	return &APIServer{
		config:  config,
		router:  mux.NewRouter(),
		logger:  logrus.New(),
		service: *service.NewService(),
	}
}

func (s *APIServer) Run() {
	srv := &http.Server{
		Addr:    s.config.HTTPAddr,
		Handler: s.router,
	}
	cfg := repository.ConfigDB{
		Host:     "localhost",
		Port:     5432,
		User:     "postgres",
		Password: "zapirus909",
		DbName:   "testdb",
	}

	dbConn := repository.NewConfigDB(cfg)
	r, err := repository.NewRepository(*dbConn)
	if err != nil {
		panic(err)
	}
	var a models.Author
	var b models.User
	res, err := r.GetAuthors()
	if len(res) == 0 {
		for i := 0; i < 10; i++ {
			a.Name = gofakeit.Name()
			s.service.AddAuthor(a)
		}
	}

	books, err := s.service.GetBooks()
	if len(books) < 10 {

		for i := 0; i < 100; i++ {
			for {
				title := gofakeit.Name() + " " + gofakeit.Word()
				_, err := s.service.GetBook(title)
				if err == nil {
					continue
				}
				id := rand.Intn(10)
				if id == 0 {
					id = 10
				}
				err = s.service.AddBook(id, title)
				if err != nil {
					log.Println("book generation error:\n", err)
					continue
				}
				id = rand.Intn(101)
				_ = s.service.RentBook(id, title)
				break
			}

		}

		count := 50
		for i := 0; i < count; i++ {
			for {
				firstName := gofakeit.FirstName()
				lastName := gofakeit.LastName()
				name := firstName + " " + lastName
				_, err := s.service.GetUser(name)
				if err == nil {
					continue
				}
				b.Name = name
				s.service.AddUser(b)
				if err != nil {
					log.Println("user generation error: \n", err)
					continue
				}
				break
			}

		}

		s.confRouter()
		s.logger.Printf("Завелись на порту %s", s.config.HTTPAddr)
		idConnClosed := make(chan struct{})
		go func() {
			sigint := make(chan os.Signal, 1)
			signal.Notify(sigint, os.Interrupt, syscall.SIGTERM)
			<-sigint
			ctx, cancel := context.WithTimeout(context.Background(), 4*time.Second)
			defer cancel()
			if err := srv.Shutdown(ctx); err != nil {
				s.logger.Fatalln(err)
			}
			close(idConnClosed)
		}()
		if err := srv.ListenAndServe(); err != nil {
			if !errors.Is(err, http.ErrServerClosed) {
				s.logger.Fatalln(err)
			}
		}
		<-idConnClosed
		s.logger.Println("Всего доброго!")
	}
}

func (s *APIServer) confRouter() {
	s.router.HandleFunc("/getuser/{title}", s.getUser()).Methods("GET")
	s.router.HandleFunc("/getusers", s.getUsers()).Methods("GET")
	s.router.HandleFunc("/adduser", s.addUser()).Methods("POST")

	s.router.HandleFunc("/getauthor/{title}", s.getAuthor()).Methods("GET")
	s.router.HandleFunc("/getauthors", s.getAuthors()).Methods("GET")
	s.router.HandleFunc("/addauthor", s.addAuthor()).Methods("POST")

	s.router.HandleFunc("/getuserbyid/{userID}", s.getUserByID()).Methods("GET")
	s.router.HandleFunc("/getauthorbyid/{authorID}", s.getAuthorByID()).Methods("GET")

	s.router.HandleFunc("/getbook/{title}", s.getBook()).Methods("GET")
	s.router.HandleFunc("/getbooks", s.getBooks()).Methods("GET")
	s.router.HandleFunc("/addbook", s.addBook()).Methods("POST")

	s.router.HandleFunc("/rented", s.RentBook()).Methods("POST")
	s.router.HandleFunc("/return", s.ReturnBook()).Methods("POST")
	s.router.HandleFunc("/topauthors", s.GetAuthorsTop()).Methods("GET")
	// swaggerUI
	s.router.HandleFunc("/swagger", s.SwaggerUI()).Methods("GET")
	s.router.HandleFunc("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("/home/huawei/Документы/golang/go-library/public"))).ServeHTTP(w, r)

	}).Methods("GET")
}

// юзеры
func (s *APIServer) getUser() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		title := mux.Vars(r)["title"]
		user, err := s.service.GetUser(title)
		if err = json.NewEncoder(w).Encode(user); err != nil {
			s.logger.Printf("Не удалось преаброзовать: %s", err)
			w.WriteHeader(http.StatusConflict)
			return
		}
	}
}

func (s *APIServer) addUser() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		var newUser models.User
		if err := json.NewDecoder(r.Body).Decode(&newUser); err != nil {
			s.logger.Printf("Не удалось преаброзовать: %s", err)
			w.WriteHeader(http.StatusConflict)
			return
		}
		result, err := s.service.AddUser(newUser)
		if err = json.NewEncoder(w).Encode(result); err != nil {
			s.logger.Printf("Не удалось преаброзовать: %s", err)
			w.WriteHeader(http.StatusConflict)
			return
		}
	}
}

func (s *APIServer) getUsers() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		res, err := s.service.GetUsers()
		if err = json.NewEncoder(w).Encode(res); err != nil {
			s.logger.Printf("Не удалось преаброзовать: %s", err)
			w.WriteHeader(http.StatusConflict)
			return
		}
	}
}

// авторы
func (s *APIServer) getAuthor() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		title := mux.Vars(r)["title"]
		res, err := s.service.GetAuthor(title)
		if err = json.NewEncoder(w).Encode(res); err != nil {
			s.logger.Printf("Не удалось преаброзовать: %s", err)
			w.WriteHeader(http.StatusConflict)
			return
		}
	}
}

func (s *APIServer) addAuthor() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		var newAuthor models.Author
		if err := json.NewDecoder(r.Body).Decode(&newAuthor); err != nil {
			s.logger.Printf("Ошибка. Не удалось запихнуть: %s", err)
			w.WriteHeader(http.StatusConflict)
			return
		}
		err := s.service.AddAuthor(newAuthor)
		if err != nil {
			s.logger.Printf("Не удалось преаброзовать: %s", err)
			w.WriteHeader(http.StatusConflict)
			return
		}
		w.Write([]byte("Автор добавлен!"))
	}
}

func (s *APIServer) getAuthors() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		res, err := s.service.GetAuthors()
		if err = json.NewEncoder(w).Encode(res); err != nil {
			s.logger.Printf("Не удалось преаброзовать: %s", err)
			w.WriteHeader(http.StatusConflict)
			return
		}
	}
}

// книги

func (s *APIServer) getBook() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		title := mux.Vars(r)["title"]
		book, err := s.service.GetBook(title)
		if err = json.NewEncoder(w).Encode(book); err != nil {
			s.logger.Printf("Не удалось преаброзовать: %s", err)
			w.WriteHeader(http.StatusConflict)
			return
		}
	}
}

func (s *APIServer) getBooks() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		res, err := s.service.GetBooks()
		if err = json.NewEncoder(w).Encode(res); err != nil {
			s.logger.Printf("Не удалось преаброзовать: %s", err)
			w.WriteHeader(http.StatusConflict)
			return
		}
	}
}

func (s *APIServer) addBook() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		var newBook models.Book
		if err := json.NewDecoder(r.Body).Decode(&newBook); err != nil {
			s.logger.Printf("Не удалось преаброзовать: %s", err)
			w.WriteHeader(http.StatusConflict)
			return
		}
		err := s.service.AddBook(newBook.Id, newBook.Title)
		if err != nil {
			s.logger.Printf("Ошибка. Не удалось запихнуть: %s", err)
			w.WriteHeader(http.StatusConflict)
			return
		}
		w.Write([]byte("Книга добавлена!"))
	}
}

func (s *APIServer) getUserByID() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		userID := mux.Vars(r)["userID"]

		userIDs, err := strconv.Atoi(userID)
		if err != nil {
			log.Fatal("Не получилось, не фортануло")
		}
		book, err := s.service.GetUserByID(userIDs)
		if err = json.NewEncoder(w).Encode(book); err != nil {
			s.logger.Printf("Не удалось преаброзовать: %s", err)
			w.WriteHeader(http.StatusConflict)
			return
		}
	}
}

func (s *APIServer) getAuthorByID() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		authorID := mux.Vars(r)["authorID"]

		authorIDs, err := strconv.Atoi(authorID)
		if err != nil {
			log.Fatal("Не получилось, не фортануло")
		}
		author, err := s.service.GetAuthorByID(authorIDs)
		if err = json.NewEncoder(w).Encode(author); err != nil {
			s.logger.Printf("Не удалось переоброзовать: %s", err)
			w.WriteHeader(http.StatusConflict)
			return
		}
	}
}

func (s *APIServer) RentBook() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var rent models.Rent
		err := json.NewDecoder(r.Body).Decode(&rent)
		if err != nil {
			log.Println("error", err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		err = s.service.RentBook(rent.UserId, rent.Title)
		if err = json.NewEncoder(w).Encode(rent); err != nil {
			s.logger.Printf("Не удалось переоброзовать: %s", err)
			w.WriteHeader(http.StatusConflict)
			return
		}
	}
}

func (s *APIServer) ReturnBook() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		var rent models.Rent
		err := json.NewDecoder(r.Body).Decode(&rent)
		if err != nil {
			log.Println("json Decoder failed", err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		err = s.service.ReturnBook(rent.UserId, rent.Title)

		if err = json.NewEncoder(w).Encode(rent); err != nil {
			s.logger.Printf("Не удалось переоброзовать: %s", err)
			w.WriteHeader(http.StatusConflict)
			return
		}
	}
}

func (s *APIServer) GetAuthorsTop() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var ct models.ValueCount
		err := json.NewDecoder(r.Body).Decode(&ct)
		if err != nil {
			log.Println("json Decoder failed", err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		res, err := s.service.GetAuthorsTop(ct.Count)
		if err = json.NewEncoder(w).Encode(res); err != nil {
			s.logger.Printf("Не удалось переоброзовать: %s", err)
			w.WriteHeader(http.StatusConflict)
			return
		}
	}
}

const (
	swaggerTemplate = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-standalone-preset.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-standalone-preset.js"></script> -->
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-bundle.js"></script> -->
    <link rel="stylesheet" href="//unpkg.com/swagger-ui-dist@3/swagger-ui.css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui.css" /> -->
	<style>
		body {
			margin: 0;
		}
	</style>
    <title>Swagger</title>
</head>
<body>
    <div id="swagger-ui"></div>
    <script>
        window.onload = function() {
          SwaggerUIBundle({
            url: "/public/swagger.json?{{.Time}}",
            dom_id: '#swagger-ui',
            presets: [
              SwaggerUIBundle.presets.apis,
              SwaggerUIStandalonePreset
            ],
            layout: "StandaloneLayout"
          })
        }
    </script>
</body>
</html>
`
)

func (s *APIServer) SwaggerUI() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		tmpl, err := template.New("swagger").Parse(swaggerTemplate)
		if err != nil {
			return
		}
		err = tmpl.Execute(w, struct {
			Time int64
		}{
			Time: time.Now().Unix(),
		})
		if err != nil {
			return
		}
	}
}
