package repository

import (
	"fmt"
	"log"

	"github.com/jmoiron/sqlx"

	"go-library/internal/models"
)

type ConfigDB struct {
	Host     string
	Port     int
	User     string
	Password string
	DbName   string
}

func NewConfigDB(cfg ConfigDB) *ConfigDB {
	return &ConfigDB{
		Host:     cfg.Host,
		Port:     cfg.Port,
		User:     cfg.User,
		Password: cfg.Password,
		DbName:   cfg.DbName,
	}

}

type Repository struct {
	db *sqlx.DB
}

func NewRepository(cfg ConfigDB) (*Repository, error) {
	db, err := sqlx.Open("postgres", fmt.Sprintf("host=%s port=%d dbname=%s user=%s password=%s sslmode=disable",
		cfg.Host, cfg.Port, cfg.DbName, cfg.User, cfg.Password))
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return &Repository{db: db}, nil
}

// функции, для работы с данными в бд

func (r *Repository) GetUser(name string) (models.User, error) {
	var res models.User
	q := "SELECT  * FROM users WHERE user_name = $1"
	err := r.db.Get(&res, q, name)
	if err != nil {
		log.Println("error get user", err)
		return res, err
	}
	return res, nil
}

func (r *Repository) AddUser(name string) error {
	add := "INSERT INTO users(user_name) VALUES($1)"
	_, err := r.db.Exec(add, name)
	if err != nil {
		return err
	}
	return nil
}

func (r *Repository) GetUsers() ([]models.User, error) {
	var res []models.User
	q := "SELECT * FROM users"
	err := r.db.Select(&res, q)
	if err != nil {
		return res, err
	}
	for i, v := range res {
		var book []models.Book
		val := "SELECT title FROM books WHERE user_id = $1"
		err = r.db.Select(&book, val, v.Id)
		if err != nil {
			return nil, err
		}
		res[i].RentedBooks = book
	}
	return res, nil
}

func (r *Repository) GetAuthor(name string) (models.Author, error) {
	var author models.Author
	res := "SELECT author_id, author_name, coalesce(read_count, 0) as read_count FROM authors WHERE author_name = $1"
	err := r.db.Get(&author, res, name)

	if err != nil {
		log.Println(err)
		return author, err
	}

	return author, nil
}

func (r *Repository) AddAuthor(name string) error {
	val := "INSERT INTO authors(author_name) VALUES($1)"
	_, err := r.db.Exec(val, name)
	if err != nil {
		return err
	}
	return nil
}

func (r *Repository) GetAuthors() ([]models.Author, error) {
	var author []models.Author
	q := "SELECT author_id, author_name, coalesce(read_count, 0) as read_count FROM authors"
	err := r.db.Select(&author, q)
	if err != nil {
		return author, err
	}
	for i, v := range author {
		var book []models.Book
		val := "SELECT title FROM books WHERE author_id = $1"
		err = r.db.Select(&book, val, v.Id)
		if err != nil {
			return nil, err
		}
		author[i].Books = book
	}
	return author, nil
}

func (r *Repository) GetBook(title string) (models.Book, error) {
	var res models.Book
	q := "SELECT book_id, title, author_id, coalesce(user_id, 0) as user_id  FROM books WHERE title = $1"
	err := r.db.Get(&res, q, title)

	if err != nil {
		log.Fatalln("error get book", err)
		return res, err
	}

	return res, nil
}

func (r *Repository) GetBooks() ([]models.Book, error) {
	var book []models.Book
	q := "SELECT book_id, title, author_id, coalesce(user_id, 0) as user_id  FROM books"
	err := r.db.Select(&book, q)

	if err != nil {
		return book, err
	}
	return book, nil
}

func (r *Repository) AddBook(authorID int, title string) error {
	q := "INSERT INTO books(title, author_id) VALUES($1, $2)"
	_, err := r.db.Exec(q, title, authorID)
	if err != nil {
		return err
	}
	return nil
}

func (r *Repository) GetUserById(userId int) (models.User, error) {
	var user models.User
	val := "SELECT * FROM users WHERE user_id = $1"
	err := r.db.Get(&user, val, userId)
	if err != nil {
		log.Println("error get user", err)
		return user, err
	}
	return user, nil
}

func (r *Repository) GetAuthorById(authorId int) (models.Author, error) {
	var author models.Author
	val := "SELECT * FROM authors WHERE author_id = $1"
	err := r.db.Get(&author, val, authorId)
	if err != nil {
		log.Println("err get author", err)
		return author, err
	}
	return author, nil
}

func (r *Repository) RentBook(userId int, title string) error {
	val := "UPDATE books SET user_id=$1 WHERE title=$2"
	_, err := r.db.Exec(val, userId, title)
	if err != nil {
		return err
	}
	book, _ := r.GetBook(title)
	q2 := "UPDATE authors SET read_count= read_count+1  WHERE author_id=$1"
	_, err = r.db.Exec(q2, book.Author)
	if err != nil {
		return err
	}
	return nil
}

func (r *Repository) ReturnBook(title string) error {
	val := "UPDATE books SET user_id=NULL WHERE title=$1"
	_, err := r.db.Exec(val, title)
	if err != nil {
		return err
	}
	return nil
}

func (r *Repository) GetTopAuthors(count int) ([]models.Author, error) {
	var author []models.Author
	val := "SELECT author_id, author_name, coalesce(read_count, 0) as read_count FROM authors ORDER BY read_count DESC LIMIT $1"
	err := r.db.Select(&author, val, count)
	if err != nil {
		return author, err
	}
	return author, nil
}
